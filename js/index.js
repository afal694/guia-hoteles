$(function(){
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $('.carousel').carousel({
    interval: 3500
  });
});
$('#modalReservar').on('hidden.bs.modal', function (e) {
  console.log("Se escondió");
  $('#btnDetalles').removeClass("disabled");
  $('#btnDetalles').addClass("btn-secondary");
  $('#btnDetalles').prop("aria-disabled",false);
})
$('#modalReservar').on('show.bs.modal',function (e){
  console.log("Se muestra");
  $('#btnDetalles').removeClass("btn-secondary");
  $('#btnDetalles').addClass("disabled");
  $('#btnDetalles').prop("aria-disabled",true);
})
$('#modalReservar').on('shown.bs.modal',function (e){
  console.log("se mostró");
})
$('#modalReservar').on('hide.bs.modal',function(e){
  console.log("se esconde");
})
