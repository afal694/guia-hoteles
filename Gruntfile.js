module.exports = function (grunt){
  require('time-grunt')(grunt);
  require('jit-grunt')(grunt,{
    rev: 'grunt-file-rev',
    useminPrepare: 'grunt-usemin'
  });

  grunt.initConfig({
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'css',
          src: ['*.scss'],
          dest: 'css',
          ext: '.css'
        }]
      }
    },

    watch: {
      files: ['css/*.scss'],
      tasks: ['css']
    },

    browserSync: {
      dev: {
        bsFiles: { //browser Files
          src: [
            'css/*.css',
            '*.html',
            'js/*.js'
          ]
        },
        options: {
          watchTask: true,
          server: {
            baseDir: "./" //directorio base para nuestro servidor
          }
        }
      }
    },

    imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: ['Imagenes/**/*.{png,jpg,gif,jpeg}'],
                    dest: 'dist/'
                }]
            }
      },

      copy: {
        html: {
          files: [{
              expand: true,
              dot: true,
              cwd: './',
              src: ['*.html'],
              dest: 'dist'
          }]
        },
        fonts: {
          files: [
            { //for openiconic
              expand: true,
              dot: true,
              cwd: 'node_modules/open-iconic/font',
              src: ['fonts/*.*'],
              dest: 'dist'},
            { //for font_awesome
              expand: true,
              dot: true,
              cwd: 'node_modules/@fortawesome/fontawesome-free',
              src: ['webfonts/*.*'],
              dest: 'dist'}
            ]
        }
      },

      clean: {
        build: {
          src: ['dist/']
        }
      },

      cssmin: {
        dist: {}
      },

      uglify: {
        dist: {}
      },

      rev: {
        options: {
          encoding: 'utf8',
          algorithm: 'md5',
          length: 20
        },
        release: {
          files: [{
            src: [
              'dist/js/*.js',
              'dist/css/*.css',
            ]
          }]
        }
      },

      concat: {
        options: {
          separator: ';'
        },
        dist: {}
      },

      useminPrepare: {
        foo: {
          dest: 'dist',
          src: ['index.html','about.html','precios.html','contacto.html']
        },
        options: {
          flow: {
            steps: {
              css: ['cssmin'],
              js: ['uglify']
            },
            post: {
              css: [{
                name: 'cssmin',
                createConfig: function(context,block){
                  var generated = context.options.generated;
                  generated.options = {
                    keepSpecialComments: 0,
                    rebase: false
                  }
                }
              }]
            }
          }
        }
      },

      usemin: {
        html: ['dist/index.html','dist/about.html','dist/precios.html','dist/contacto.html'],
        options: {
          assetsDir: ['dist','dist/css','dist/js']
        }
      }

  });

  // grunt.loadNpmTasks('grunt-browser-sync'); Desde que se usa jit-grunt ya no se usan estos!!!
  // grunt.loadNpmTasks('grunt-contrib-watch');
  // grunt.loadNpmTasks('grunt-contrib-sass');
  // grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.registerTask('img:compress',['imagemin']);
  grunt.registerTask('css',['sass']);
  grunt.registerTask('default',['browserSync','watch']);
  grunt.registerTask('build',[
    'clean',
    'copy',
    'imagemin',
    'useminPrepare',
    'concat',
    'cssmin',
    'uglify',
    'rev',
    'usemin'
  ])
};
