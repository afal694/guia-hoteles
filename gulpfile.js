'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();


sass.compiler = require('node-sass');


gulp.task('sass',function(){
  return gulp.src('./css/*.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(gulp.dest('./css'));
})

gulp.task('sass:watch', function () {
  gulp.watch('./css/*.scss', gulp.series('sass'));
});

// Static server
gulp.task('browser-sync', function() {
  var files = ['./*.html','./css/*.css','./Imagenes/**/*{png,jpeg,jpg,gif}','./js/*.js']
    browserSync.init(files,{
        server: {
            baseDir: "./"
        }
    });
});

function browserSync(cb){

  cb();
}

// // Static server
// gulp.task('browser-sync', function() {
//     browserSync.init({
//         server: {
//             baseDir: "./"
//         }
//     });
// });

gulp.task('default',gulp.paralell('browser-sync','sass:watch'));
